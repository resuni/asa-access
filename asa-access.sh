#!/bin/bash
## Script launches ASDM using Java web start (IcedTea) from a user-specified IP address.
# Connects to console if 'console' is specified instead of an IP address

if [ $1 == "console" ]; then
 screen /dev/ttyUSB0 9600
else
 printf "If IcedTea hangs here, please ensure the ASA is accessible by pointing your browser at https://$1/admin/ \n"
 javaws https://$1/admin/public/asdm.jnlp
fi
